package ru.t1.akolobov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.user.UserNotFoundException;
import ru.t1.akolobov.tm.model.User;

public final class UserDisplayProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-display-profile";

    @NotNull
    public static final String DESCRIPTION = "Display current user profile.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY USER PROFILE]");
        @NotNull final User user = getAuthService().getUser();
        displayUser(user);
    }

    @Override
    protected void displayUser(@Nullable final User user) {
        super.displayUser(user);
        if (user == null) throw new UserNotFoundException();
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
