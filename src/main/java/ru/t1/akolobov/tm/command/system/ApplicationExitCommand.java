package ru.t1.akolobov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "exit";

    @NotNull
    public static final String DESCRIPTION = "Exit application.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
